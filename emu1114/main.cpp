// emu1114/main.cpp 2016/4/9
#if defined(TARGET_LPC1768)
#include "EMU111x.h"
#include "SDStorage.h"
#include "F32RFileSystem.h"
#include "ROMSLOT.h"

DigitalOut led1(LED1),led2(LED2);
RawSerial pc(MBED_UARTUSB);
#ifdef USE_SD
SDStorage storage(MBED_SPI0);
F32RFileSystem local(&storage, "local");
#else
LocalFileSystem local("local");
#endif
ROMSLOT slot;

class MyEMU1114 : public EMU111x {
    virtual void DigitalWrite_Callback(int port, int pin, int value) {
        switch(port<<8|pin) {
            case 1<<8|5: led1 = value; break; // PIO1_5(LED1)
            case 0<<8|7: led2 = value; break; // PIO0_7(LED2)
        }
    }
    virtual void SerialPutc_Callback(int ch, uint8_t c) { pc.putc(c); }
    virtual int SerialGetc_Callback(int ch) { return pc.getc(); }
    virtual int SerialReadable_Callback(int ch) { return pc.readable(); }
};

uint8_t* load(size_t limit, const char* filename) {
    pc.printf("loading[%s]", filename);
    FileHandle* fh = local.open(filename, O_RDONLY);
    MBED_ASSERT(fh);
    uint32_t addr = slot.New(limit);
    for(size_t n = 0; n <= limit;) {
        uint8_t buf[256];
        size_t r = fh->read(buf, sizeof(buf));
        if (r == 0) {
            pc.printf(" %u bytes\n", n);
            break;
        }
        slot.Write(addr + n, (const uint8_t*)buf, sizeof(buf));
        n += r;
        pc.putc('.');
    }
    fh->close();
    return (uint8_t*)addr;
}

int main() {
    pc.baud(115200);
    pc.puts("mbed LPC1114 emulator pre-alpha version 0.01\n");

    MyEMU1114 mcu;
    mcu.assign_flash(load(0x8000, "LPC1114.IMG"));

    mcu.reset();
    for(;;) {
        mcu.run(100);
        mcu.clock_in(250);
    }
    /* NOTREACHED */
}
#endif // TARGET_LPC1768

