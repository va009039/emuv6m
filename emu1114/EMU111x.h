// EMU111x.h 2015/8/19
#include "BaseV6M.h"
#include "EMUInterface.h"
#pragma once

class EMU111x;

class EMU111x_SYSCON {
public:
    EMU111x_SYSCON();
    void poke32(uint32_t a, uint32_t d);
    uint32_t peek32(uint32_t a);
};

class EMU111x_IOCON {
public:
    EMU111x_IOCON();
    void poke32(uint32_t a, uint32_t d);
    uint32_t peek32(uint32_t a);

private:
    uint32_t RESET_PIO0_0;
    uint32_t PIO0_1;
    uint32_t PIO1_8;
    uint32_t PIO0_2;
    uint32_t PIO0_3;
    uint32_t PIO0_4;
    uint32_t PIO0_5;
    uint32_t PIO1_9;

    uint32_t PIO0_6;
    uint32_t PIO0_7;

    uint32_t PIO0_8;
    uint32_t PIO0_9;
    uint32_t SWCLK_PIO0_10;
    uint32_t PIO1_10;

    uint32_t R_PIO0_11;
    uint32_t R_PIO1_0;
    uint32_t R_PIO1_1;
    uint32_t R_PIO1_2;

    uint32_t SWDIO_PIO_3;
    uint32_t PIO1_4;
    uint32_t PIO1_11;

    uint32_t PIO1_5;
    uint32_t PIO1_6;
    uint32_t PIO1_7;

    uint32_t SCK_LOC;
};

class EMU111x_GPIO {
public:
    EMU111x_GPIO(EMU111x& mcu, int port);
    void poke32(uint32_t a, uint32_t d);
    uint32_t peek32(uint32_t a);

private:
    EMU111x& mcu;
    int port;
    uint32_t data;
    uint32_t dir;
};

class EMU111x_TMR32B {
public:
    EMU111x_TMR32B(int ch);
    void clock_in(uint32_t n = 1);
    void poke32(uint32_t a, uint32_t d);
    uint32_t peek32(uint32_t a);

private:
    int ch;
    uint32_t tc;
};

class EMU111x_UART {
public:
    EMU111x_UART(EMU111x& mcu);
    void poke32(uint32_t a, uint32_t d);
    uint32_t peek32(uint32_t a);

private:
    EMU111x& mcu;
    uint32_t LCR;
    uint32_t FDR;
};

class EMU111x_I2C {
public:
    EMU111x_I2C(EMU111x& mcu);
    void poke32(uint32_t a, uint32_t d);
    uint32_t peek32(uint32_t a);

private:
    EMU111x& mcu;
    uint32_t con;
    uint32_t stat;
    uint8_t i2c_addr;
    uint8_t i2c_data[16];
    int i2c_pos;
};

class EMU111x_SPI {
public:
    EMU111x_SPI(EMU111x& mcu, int ch);
    void poke32(uint32_t a, uint32_t d);
    uint32_t peek32(uint32_t a);

private:
    EMU111x& mcu;
    uint32_t cr0;
    uint32_t cr1;
    int ch;
    int dr;
};

class EMU111x_NVIC {
public:
    void poke32(uint32_t a, uint32_t d);

private:
    uint32_t iser;
};

class EMU111x : public EMUInterface, public BaseV6M {
public:
    EMU111x();
    void assign_flash(const uint8_t* addr) { flash = addr; }
    void assign_rom(uint8_t* addr) { rom = addr; }
    void clock_in(uint32_t n);
    void trace();

protected:
    virtual void poke32(uint32_t a, uint32_t d);
    virtual uint32_t peek32(uint32_t a);
    virtual void poke8(uint32_t a, uint8_t b);
    virtual uint8_t peek8(uint32_t a);

private:
    const uint8_t* flash;
    const uint8_t* rom;
    uint8_t* ram;

    EMU111x_SYSCON syscon;
    EMU111x_IOCON iocon;
    EMU111x_TMR32B tmr32b1;
    EMU111x_UART _uart;
    EMU111x_I2C _i2c;
    EMU111x_SPI _spi0;
    EMU111x_SPI _spi1;
    EMU111x_GPIO gpio0;
    EMU111x_GPIO gpio1;
    EMU111x_NVIC nvic;

    friend class EMU111x_UART;
    friend class EMU111x_I2C;
    friend class EMU111x_SPI;
    friend class EMU111x_GPIO;
};

