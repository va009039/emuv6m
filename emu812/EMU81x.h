// EMU81x.h 2015/8/12
#pragma once
#include "BaseV6M.h"
#include "EMUInterface.h"

class EMU81x;

class EMU81x_USART {
public:
    EMU81x_USART(EMU81x& mcu, int ch);
    void poke32(uint32_t a, uint32_t d);
    uint32_t peek32(uint32_t a);

private:
    EMU81x& mcu;
    int ch;
    uint32_t CFG;
    uint32_t STAT;
    uint32_t INTENCLR;
    uint32_t BRG;
};

class EMU81x_MRT {
public:
    EMU81x_MRT();
    void clock_in(uint32_t n = 1);
    void poke32(uint32_t a, uint32_t d);
    uint32_t peek32(uint32_t a);
private:
    uint32_t timer0;
};

class EMU81x_IOCON {
public:
    EMU81x_IOCON();
    void poke32(uint32_t a, uint32_t d);
    uint32_t peek32(uint32_t a);

private:
    uint32_t PIO0_17;
    uint32_t PIO0_13;
    uint32_t PIO0_12;
    uint32_t PIO0_5;
    uint32_t PIO0_4;
    uint32_t PIO0_3;
    uint32_t PIO0_2;
    uint32_t PIO0_11;
    uint32_t PIO0_10;
    uint32_t PIO0_16;
    uint32_t PIO0_15;
    uint32_t PIO0_1;
    uint32_t PIO0_9;
    uint32_t PIO0_8;
    uint32_t PIO0_7;
    uint32_t PIO0_6;
    uint32_t PIO0_0;
    uint32_t PIO0_14;
};

class EMU81x_SYSCON {
public:
    EMU81x_SYSCON();
    void poke32(uint32_t a, uint32_t d);
    uint32_t peek32(uint32_t a);
};

class EMU81x_SWM {
public:
    EMU81x_SWM();
    void poke32(uint32_t a, uint32_t d);
    uint32_t peek32(uint32_t a);

private:
    uint32_t pinassign[9];
    uint32_t pinenable0;
 };

class EMU81x_GPIO {
public:
    EMU81x_GPIO(EMU81x& mcu);
    void poke32(uint32_t a, uint32_t d);
    uint32_t peek32(uint32_t a);

private:
    uint32_t dir0;
    uint32_t data0;
    EMU81x& mcu;
};

class EMU81x_I2C {
public:
    EMU81x_I2C(EMU81x& mcu);
    void poke32(uint32_t a, uint32_t d);
    uint32_t peek32(uint32_t a);

private:
    EMU81x& mcu;
    uint32_t cfg;
    int mststate;
    uint8_t mstdat;
    uint8_t i2c_addr;
    uint8_t i2c_data[16];
    int i2c_pos;
};

class EMU81x_SPI {
public:
    EMU81x_SPI(EMU81x& mcu, int ch);
    void poke32(uint32_t a, uint32_t d);
    uint32_t peek32(uint32_t a);

private:
    EMU81x& mcu;
    int ch;
    uint8_t rxdat;
};

class EMU81x_NVIC {
public:
    void poke32(uint32_t a, uint32_t d);

private:
    uint32_t iser;
};

class EMU81x_SCB {
public:
    EMU81x_SCB();
    void poke32(uint32_t a, uint32_t d);
    uint32_t peek32(uint32_t a);

private:
    uint32_t vtor;
};

class EMU81x : public EMUInterface, public BaseV6M {
public:
    EMU81x();
    virtual ~EMU81x();
    void assign_flash(const uint8_t* addr) { flash = addr; }
    void assign_rom(const uint8_t* addr) { rom = addr; }
    void clock_in(uint32_t n);
    void trace();

protected:
    virtual void poke32(uint32_t a, uint32_t d);
    virtual uint32_t peek32(uint32_t a);
    virtual void poke8(uint32_t a, uint8_t b);
    virtual uint8_t peek8(uint32_t a);

private:
    const uint8_t* flash;
    const uint8_t* rom;
    uint8_t* ram;
    EMU81x_USART _uart0;
    EMU81x_USART _uart1;
    EMU81x_USART _uart2;
    EMU81x_MRT mrt;
    EMU81x_IOCON iocon;
    EMU81x_SYSCON syscon;
    EMU81x_SWM swm;
    EMU81x_GPIO gpio;
    EMU81x_I2C _i2c;
    EMU81x_SPI _spi0;
    EMU81x_SPI _spi1;
    EMU81x_SCB scb;
    EMU81x_NVIC nvic;

    friend class EMU81x_USART;
    friend class EMU81x_GPIO;
    friend class EMU81x_I2C;
    friend class EMU81x_SPI;
};
