// EMUInterface.h 2015/8/3
#pragma once
#include "mbed.h"

class EMUInterface {
protected:
    virtual void SerialPutc_Callback(int ch, uint8_t c) {}
    virtual int SerialGetc_Callback(int ch) { return -1; }
    virtual int SerialReadable_Callback(int ch) { return 0; }
    virtual void DigitalWrite_Callback(int port, int pin, int value) {}
    virtual int DigitalRead_Callback(int port, int pin) { return 0; }
    virtual int I2CWrite_Callback(uint8_t addr, const uint8_t* data, int size) { return 0; }
    virtual int I2CRead_Callback(uint8_t addr, uint8_t* data, int size) { return 0; }
    virtual int SPIWrite_Callback(int ch, int value) { return 0; }
};


