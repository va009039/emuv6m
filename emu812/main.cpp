// emu812/main.cpp 2016/4/9
#if defined(TARGET_LPC1768)
#include "EMU81x.h"
#include "SDStorage.h"
#include "F32RFileSystem.h"
#include "ROMSLOT.h"

DigitalOut led1(LED1),led2(LED2),led3(LED3);
RawSerial pc(MBED_UARTUSB);
RawSerial uart1(MBED_UART0); // p9,p10
I2C i2c(MBED_I2C0); // p28,p27
#if defined(USE_SD)
SDStorage storage(MBED_SPI0);
F32RFileSystem local(&storage, "local");
#else
LocalFileSystem local("local");
#endif
ROMSLOT slot;

class MyEMU812 : public EMU81x {
    virtual void DigitalWrite_Callback(int port, int pin, int value) {
        switch(pin) {
            case 16: led1 = value; break; // PIO0_16 LED_BLUE
            case 17: led2 = value; break; // PIO0_17 LED_GREEN
            case 7: led3 = value; break; // PIO0_7 LED_RED
        }
    }
    virtual void SerialPutc_Callback(int ch, uint8_t c) {
        switch(ch) {
            case 0: pc.putc(c); break;
            case 1: uart1.putc(c); break;
        }
    }
    virtual int SerialGetc_Callback(int ch) {
        switch(ch) {
            case 0: return pc.getc();
            case 1: return uart1.getc();
        }
        return -1;
    }
    virtual int SerialReadable_Callback(int ch) {
        switch(ch) {
            case 0: return pc.readable();
            case 1: return uart1.readable();
        }
        return 0;
    }
    virtual int I2CWrite_Callback(uint8_t addr, const uint8_t* data, int size) {
        return i2c.write(addr, (char*)data, size);
    }
    virtual int I2CRead_Callback(uint8_t addr, uint8_t* data, int size) {
        return i2c.read(addr, (char*)data, size);
    }
};

uint8_t* load(size_t limit, const char* filename) {
    pc.printf("loading[%s]", filename);
    FileHandle* fh = local.open(filename, O_RDONLY);
    MBED_ASSERT(fh);
    uint32_t addr = slot.New(limit);
    for(size_t n = 0; n <= limit;) {
        uint8_t buf[256];
        size_t r = fh->read(buf, sizeof(buf));
        if (r == 0) {
            pc.printf(" %u bytes\n", n);
            break;
        }
        slot.Write(addr + n, (const uint8_t*)buf, sizeof(buf));
        n += r;
        pc.putc('.');
    }
    fh->close();
    return (uint8_t*)addr;
}

int main() {
    pc.baud(115200);
    pc.puts("mbed LPC812 emulator pre-alpha version 0.01\n");

    MyEMU812 mcu;
    mcu.assign_flash(load(0x4000, "LPC812.IMG"));
    mcu.assign_rom(load(0x2000, "LPC812.ROM"));

    mcu.reset();
    for(;;) {
        mcu.run(100);
        mcu.clock_in(10000);
    }
    /* NOTREACHED */
}
#endif // TARGET_LPC1768
