// test_v6m.h 2015/8/26
#include <map>
#include <assert.h>
#include "BaseV6M.h"

class TEST_V6M;

class REG1 {
    TEST_V6M& v6m;
    int n;
public:
    REG1(TEST_V6M& v6m_, int n_):v6m(v6m_),n(n_) { }
    REG1& operator=(uint32_t data) {
        write(data);
        return *this;
    }
    bool operator==(uint32_t data) {
        return read() == data;
    }
    uint32_t& read();
    uint32_t& write(uint32_t data);
};

class TEST_V6M : public BaseV6M {
public:
    REG1 SP,LR,PC,PSR;
    std::map<uint32_t,uint8_t>mem;
    TEST_V6M():SP(*this,13),LR(*this,14),PC(*this,15),PSR(*this,16) {}
    uint32_t memA(uint32_t a, int n) {
        uint32_t d = 0;
        for(int i = 0; i < n; i++) {
            d |= peek8(a+i)<<(8*i);
        }
        return d;
    }
    virtual void poke32(uint32_t a, uint32_t d) { poke16(a, d); poke16(a+2, d>>16); }
    virtual uint32_t peek32(uint32_t a) { return peek16(a)|peek16(a+2)<<16; }
    virtual void poke8(uint32_t a, uint8_t b) { mem[a] = b; }
    virtual uint8_t peek8(uint32_t a) { return mem[a]; }
    uint32_t& readReg(int n) { return R[n]; }
    uint32_t& writeReg(int n, uint32_t data) { R[n] = data; return R[n]; }
    friend class REG1;
};

uint32_t& REG1::read() {
    return v6m.readReg(n);
}

uint32_t& REG1::write(uint32_t data) {
    return v6m.writeReg(n, data);
}


class intn_t {
public:
    uint32_t value;
    int width;
    intn_t() {
        value = 0;
        width = 0;
    }
    intn_t(const intn_t& i) {
        value = i.value;
        width = i.width;
    }
    intn_t(const char* s) {
        width = strlen(s);
        value = 0;
        for(int i = 0; i < width; i++) {
            value <<= 1;
            assert(s[i] == '0' || s[i] == '1');
            if (s[i] == '1') {
                value |= 1;
            }
        }
    }
    intn_t(uint32_t value_, int width_, int base = 0) {
        assert(width_ >= 1 && width_ <= 32);
        assert(base >= 0 && base <= 31);
        value = (value_>>base) & ((1<<width_)-1);
        width = width_;
        assert(value < (1UL<<width));
    }
    intn_t& append(const intn_t& i) {
        value <<= i.width;
        value |= i.value;
        width += i.width;
        return *this;
    }
    int32_t signed_extend() {
        if (value & (1<<(width-1))) {
            return value - (1<<width);
        }
        return value;
    }
    uint32_t unsiged_extend() {
         return value;
    }
    uint32_t mask() { return (1<<width)-1; }
    intn_t EOR(intn_t& i) {
        intn_t r(*this);
        r.value ^= i.value;
        assert(r.width == i.width);
        return r;
    }
};

static intn_t NOT(const intn_t& i) {
    intn_t r = i;
    r.value = (~r.value) & r.mask();
    return r;
}

static int32_t SignedExtend(const intn_t& S, const intn_t& T1, const intn_t& T2, const intn_t& imm10, const intn_t& imm11, const intn_t& zero) {
    assert(S.width == 1 && T1.width == 1 && T2.width == 1);
    assert(imm10.width == 10 && imm11.width == 11);
    assert(zero.width == 1 && zero.value == 0);
    intn_t i;
    i.append(S);
    i.append(T1);
    i.append(T2);
    i.append(imm10);
    i.append(imm11);
    i.append(zero);
    assert(i.width == 25);
    return i.signed_extend();
}

static uint32_t ZeroExtend(const intn_t& imm8, int n = 32) {
    assert(imm8.width == 8);
    assert(n == 32);
    intn_t i;
    i.append(imm8);
    return i.value;
}

static uint32_t UInt(const intn_t& Rd) {
    return Rd.value;
}

