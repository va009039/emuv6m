// test_v6m_branch.cpp 2015/8/27
#ifdef TEST_V6M_BRANCH
#include "mbed.h"
#include "mytest.h"
#include "test_v6m.h"

DigitalOut led1(LED1);
RawSerial pc(USBTX,USBRX);

TEST(BaseV6M,mov_immediate) {
    TEST_V6M v6m;
    for(uint16_t code = 0x2000; code <= 0x27ff; code++) {
        const auto pc =0x1000;
        const auto sp = 0x20000400;
        v6m.poke32(0x0000, sp);
        v6m.poke32(0x0004, pc+1);
        v6m.poke32(pc, code);
        v6m.reset();
        v6m.run(1);
        intn_t Rd(code, 3, 8);
        intn_t imm8(code, 8, 0);
        auto d = UInt(Rd);
        auto imm32 = ZeroExtend(imm8, 32);
        //TEST_PRINT("code=%04x d=%d imm32=%08x", code, d, imm32);
        ASSERT_TRUE(v6m.PC == pc+4);
        ASSERT_TRUE(v6m.SP == sp);
        ASSERT_TRUE(imm32 == v6m.readReg(d));
    }
}

TEST(BaseV6M,test_c_bl) {
    TEST_V6M v6m;
    uint16_t code_range[] = {0xf000, 0xf100, 0xf200, 0xf2ff, 0xf300, 0xf3ff, 0xf400, 0xf4ff, 0xf5ff, 0xf6ff, 0xf7ff};
    for(auto code : code_range) {
        uint16_t code2nd_range[] = {0xf800, 0xf8ff, 0xff00, 0xffff, 0xd000, 0xdfff, 0xf000, 0xf7ff};
        for(auto code2nd : code2nd_range) {
            const auto pc = 0x10000000;
            const auto sp = 0x20000400;
            v6m.poke32(0x0000, sp);
            v6m.poke32(0x0004, pc+1);
            v6m.poke32(pc, code|code2nd<<16);
            v6m.reset();
            v6m.run(1);
            //TEST_PRINT("%08x: %08x", pc, v6m.peek32(pc));
            ASSERT_TRUE(v6m.peek32(pc) == (code|code2nd<<16));
            ASSERT_TRUE(v6m.SP == sp);
            ASSERT_TRUE(v6m.LR == pc+4+1);
            intn_t S(code, 1, 10);
            intn_t imm10(code, 10, 0);
            intn_t J1(code2nd, 1, 13);
            intn_t J2(code2nd, 1, 11);
            intn_t imm11(code2nd, 11, 0);
            intn_t T1 = NOT(J1.EOR(S));
            intn_t T2 = NOT(J2.EOR(S));
            auto imm32 = SignedExtend(S,T1,T2,imm10,imm11,intn_t("0"));
            //TEST_PRINT("%04x %04x imm32=%08x pc=%08x", code, code2nd, imm32, v6m.PC.read());
            ASSERT_TRUE(v6m.PC == (pc+imm32+4))
        }
    }
}

TEST(BaseV6M,test_c_bl_d000) {
    TEST_V6M v6m;
    uint16_t code_range[] = {0xf000, 0xf100, 0xf200, 0xf2ff, 0xf300, 0xf3ff, 0xf400, 0xf4ff, 0xf5ff, 0xf6ff, 0xf7ff};
    for(auto code : code_range) {
        for(uint32_t code2nd = 0xd000; code2nd <= 0xdfff; code2nd++) {
            const auto pc = 0x10000000;
            const auto sp = 0x20000400;
            v6m.poke32(0x0000, sp);
            v6m.poke32(0x0004, pc+1);
            v6m.poke32(pc, code|code2nd<<16);
            v6m.reset();
            v6m.run(1);
            ASSERT_TRUE(v6m.peek32(pc) == (code|code2nd<<16));
            ASSERT_TRUE(v6m.SP == sp);
            ASSERT_TRUE(v6m.LR == pc+4+1);
            intn_t S(code, 1, 10);
            intn_t imm10(code, 10, 0);
            intn_t J1(code2nd, 1, 13);
            intn_t J2(code2nd, 1, 11);
            intn_t imm11(code2nd, 11, 0);
            intn_t T1 = NOT(J1.EOR(S));
            intn_t T2 = NOT(J2.EOR(S));
            auto imm32 = SignedExtend(S,T1,T2,imm10,imm11,intn_t("0"));
            //TEST_PRINT("%04x %04x imm32=%08x pc=%08x", code, code2nd, imm32, v6m.PC.read());
            ASSERT_TRUE(v6m.PC == (pc+imm32+4))
        }
    }
}

TEST(BaseV6M,test_c_bl_f000) {
    TEST_V6M v6m;
    uint16_t code_range[] = {0xf000, 0xf100, 0xf200, 0xf2ff, 0xf300, 0xf3ff, 0xf400, 0xf4ff, 0xf5ff, 0xf6ff, 0xf7ff};
    for(auto code : code_range) {
        for(uint32_t code2nd = 0xf000; code2nd <= 0xffff; code2nd++) {
            const auto pc = 0x10000000;
            const auto sp = 0x20000400;
            v6m.poke32(0x0000, sp);
            v6m.poke32(0x0004, pc+1);
            v6m.poke32(pc, code|code2nd<<16);
            v6m.reset();
            v6m.run(1);
            ASSERT_TRUE(v6m.peek32(pc) == (code|code2nd<<16));
            ASSERT_TRUE(v6m.SP == sp);
            ASSERT_TRUE(v6m.LR == pc+4+1);
            intn_t S(code, 1, 10);
            intn_t imm10(code, 10, 0);
            intn_t J1(code2nd, 1, 13);
            intn_t J2(code2nd, 1, 11);
            intn_t imm11(code2nd, 11, 0);
            intn_t T1 = NOT(J1.EOR(S));
            intn_t T2 = NOT(J2.EOR(S));
            auto imm32 = SignedExtend(S,T1,T2,imm10,imm11,intn_t("0"));
            //TEST_PRINT("%04x %04x imm32=%08x pc=%08x", code, code2nd, imm32, v6m.PC.read());
            ASSERT_TRUE(v6m.PC == (pc+imm32+4))
        }
    }
}

int main() {
    pc.baud(115200);
    //pc.printf("%s\n", __FILE__);

    RUN_ALL_TESTS();
    while(1) {
        led1 = !led1;
        wait_ms(200);
    }
}
#endif // TEST_V6M_BRANCH
