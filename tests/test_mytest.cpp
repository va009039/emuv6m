// test_mytest.cpp 2015/8/20
#ifdef TEST_MYTEST
#include "mbed.h"
#include "mytest.h"

DigitalOut led1(LED1);
RawSerial pc(USBTX,USBRX);

TEST(mytest,test1) {
    int a = 1;
    int b = 2;
    int c = a + b;
    ASSERT_TRUE(c == 3);
}

int main() {
    pc.baud(115200);
    pc.printf("%s\n", __FILE__);

    RUN_ALL_TESTS();
    while(1) {
        led1 = !led1;
        wait_ms(200);
    }
}
#endif // TEST_MYTEST




