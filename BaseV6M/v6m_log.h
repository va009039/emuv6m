// v6m_log.h 2015/8/4
#pragma once
#include "mbed.h"

#ifndef V6M_LOG_LEVEL
#define V6M_LOG_LEVEL 1
#endif

#if V6M_LOG_LEVEL >= 4
#define V6M_DEBUG(...) do{printf(__VA_ARGS__);printf("\n");}while(0)
#else
#define V6M_DEBUG(...) while(0)
#endif

#if V6M_LOG_LEVEL >= 3
#define V6M_INFO(...) do{printf(__VA_ARGS__);printf("\n");}while(0)
#else
#define V6M_INFO(...) while(0)
#endif

#if V6M_LOG_LEVEL >= 2
#define V6M_WARN(...) do{printf(__VA_ARGS__);printf("\n");}while(0)
#else
#define V6M_WARN(...) while(0)
#endif

#if V6M_LOG_LEVEL >= 1
#define V6M_ERROR(...) do{printf(__VA_ARGS__);printf("\n");}while(0)
#else
#define V6M_ERROR(...) while(0)
#endif

#define V6M_ASSERT(A) MBED_ASSERT(A)

